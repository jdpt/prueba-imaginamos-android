package pl.surecase.eu;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;

public class MyDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(3, "co.imaginamos.pruebajuanpoveda.database");

        //ProductoDb
        Entity productoDb = schema.addEntity("ProductoDb");
        Property productoId = productoDb.addIdProperty().getProperty();
        productoDb.setTableName("productoDb");
        productoDb.addStringProperty("name");
        productoDb.addStringProperty("image");
        productoDb.addStringProperty("summary");
        productoDb.addStringProperty("price");
        productoDb.addStringProperty("title");
        productoDb.addStringProperty("artist");
        productoDb.addStringProperty("currency");

        //CategoriaDb
        Entity categoriaDb = schema.addEntity("CategoriaDb");
        Property categoriaId = categoriaDb.addIdProperty().getProperty();
        categoriaDb.setTableName("categoriaDb");
        categoriaDb.addStringProperty("nombre");

        Property productoFk = productoDb.addLongProperty("categoriaId").notNull().getProperty(); //Relacion de producto a categoria.
        productoDb.addToOne(categoriaDb, productoFk);

        ToMany catToProducto = categoriaDb.addToMany(productoDb, productoId); //Relacion de categoria a producto
        catToProducto.setName("productos");

        categoriaDb.implementsSerializable();
        productoDb.implementsSerializable();

        new DaoGenerator().generateAll(schema, args[0]);
    }
}
