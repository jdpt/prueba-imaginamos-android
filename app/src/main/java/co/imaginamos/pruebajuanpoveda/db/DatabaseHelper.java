package co.imaginamos.pruebajuanpoveda.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import co.imaginamos.pruebajuanpoveda.database.CategoriaDb;
import co.imaginamos.pruebajuanpoveda.database.CategoriaDbDao;
import co.imaginamos.pruebajuanpoveda.database.DaoMaster;
import co.imaginamos.pruebajuanpoveda.database.DaoSession;
import co.imaginamos.pruebajuanpoveda.database.ProductoDb;
import co.imaginamos.pruebajuanpoveda.database.ProductoDbDao;
import co.imaginamos.pruebajuanpoveda.pojo.Entry;
import co.imaginamos.pruebajuanpoveda.pojo.Feed;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class DatabaseHelper extends DaoMaster.OpenHelper {

    private final String TAG = DatabaseHelper.class.getSimpleName();
    private CategoriaDbDao categoriaDbDao;
    private ProductoDbDao productoDbDao;
    private SQLiteDatabase db;
    private DaoSession daoSession;

    public DatabaseHelper(Context context) {
        super(context, "prueba.db", null);

        db = this.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();

        productoDbDao = daoSession.getProductoDbDao();
        categoriaDbDao = daoSession.getCategoriaDbDao();
    }

    //Cerrar la DB

    public void close() {
        if (db.isOpen()) {
            db.close();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "Upgrading schema from version " + oldVersion + " to " + newVersion + " by dropping all tables");
        DaoMaster.dropAllTables(db, true);
        DaoMaster.createAllTables(db, false);
    }

    public List<CategoriaDb> findAllCategorias(){
        List<CategoriaDb> ubics = categoriaDbDao.loadAll();
        return ubics;
    }

    public List<ProductoDb> findAllProductos(){
        List<ProductoDb> ubics = productoDbDao.loadAll();
        return ubics;
    }

    public void deleteProductos(){
        productoDbDao.deleteAll();
    }

    public void deleteCategorias(){
        categoriaDbDao.deleteAll();
    }

    public List<CategoriaDb> findCategoriaByNombre(String nombre){
        return categoriaDbDao.queryBuilder().where(CategoriaDbDao.Properties.Nombre.eq(nombre)).list();
    }

    public void insertProductos(Feed feed){

        List<ProductoDb> prods = new ArrayList<>();

        for(Entry e : feed.getEntry()) {

            ProductoDb prodDb = new ProductoDb();

            prodDb.setId(Long.parseLong(e.getId().getAttributes().getImId()));
            prodDb.setArtist(e.getImArtist().getLabel());
            prodDb.setCategoriaId(Long.parseLong(e.getCategory().getAttributes().getImId()));
            prodDb.setImage(e.getImImage().get(2).getLabel());
            prodDb.setName(e.getImName().getLabel());
            prodDb.setPrice(e.getImPrice().getAttributes().getAmount());
            prodDb.setCurrency(e.getImPrice().getAttributes().getCurrency());
            prodDb.setSummary(e.getSummary().getLabel());
            prodDb.setTitle(e.getTitle().getLabel());

            if (!prods.contains(prodDb)) {

                prods.add(prodDb);

            }
        }

        for(ProductoDb pdb : prods) {

            daoSession.insertOrReplace(pdb);

        }


    }

    public void insertCategorias(Feed feed){

        List<CategoriaDb> cats = new ArrayList<>();

        for(Entry e : feed.getEntry()){

            if(!cats.contains(e.getCategory())){

                CategoriaDb catDb = new CategoriaDb();

                catDb.setId(Long.parseLong(e.getCategory().getAttributes().getImId()));
                catDb.setNombre(e.getCategory().getAttributes().getLabel());

                cats.add(catDb);
            }
        }

        for(CategoriaDb cdb : cats) {

            daoSession.insertOrReplace(cdb);

        }

            //System.out.println("size productos insertados: " + cdb.getNombre() + cdb.getProductos().size());
            System.out.println("find all prod: " + findAllProductos().size());

    }

    public List<ProductoDb> findProductosByCategoria(long catId){

        List<ProductoDb> prods = new ArrayList<>();

        for(ProductoDb p: findAllProductos()){
            if(p.getCategoriaId() == catId){
                prods.add(p);
            }
        }

        return prods;
    }

    public ProductoDb findProductoById(long id){

        ProductoDb ub = null;

        for(ProductoDb u : findAllProductos()){

            if(u.getId() == id){
                ub = u;
                break;
            }

        }

        return ub;
    }

}
