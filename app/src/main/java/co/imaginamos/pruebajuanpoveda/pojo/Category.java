package co.imaginamos.pruebajuanpoveda.pojo;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class Category {

    private CategoryAttrib attributes;

    public CategoryAttrib getAttributes() {
        return attributes;
    }

    public void setAttributes(CategoryAttrib attributes) {
        this.attributes = attributes;
    }
}
