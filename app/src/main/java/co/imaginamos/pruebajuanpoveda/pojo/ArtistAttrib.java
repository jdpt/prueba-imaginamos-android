package co.imaginamos.pruebajuanpoveda.pojo;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class ArtistAttrib {

    private String href;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
