package co.imaginamos.pruebajuanpoveda.pojo;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class ImImage {

    private String label;
    private ImImageAttrib attributes;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public ImImageAttrib getAttributes() {
        return attributes;
    }

    public void setAttributes(ImImageAttrib attributes) {
        this.attributes = attributes;
    }
}
