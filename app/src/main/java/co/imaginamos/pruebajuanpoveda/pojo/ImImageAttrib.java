package co.imaginamos.pruebajuanpoveda.pojo;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class ImImageAttrib {

    private String height;

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}
