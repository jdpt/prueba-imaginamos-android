package co.imaginamos.pruebajuanpoveda.pojo;

import java.util.List;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class Feed {

    private Author author;
    private Label updated;
    private Label rights;
    private Label title;
    private Label icon;
    private Label id;
    private List<Entry> entry;
    private List<Link> link;

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Label getUpdated() {
        return updated;
    }

    public void setUpdated(Label updated) {
        this.updated = updated;
    }

    public Label getRights() {
        return rights;
    }

    public void setRights(Label rights) {
        this.rights = rights;
    }

    public Label getTitle() {
        return title;
    }

    public void setTitle(Label title) {
        this.title = title;
    }

    public Label getIcon() {
        return icon;
    }

    public void setIcon(Label icon) {
        this.icon = icon;
    }

    public Label getId() {
        return id;
    }

    public void setId(Label id) {
        this.id = id;
    }

    public List<Entry> getEntry() {
        return entry;
    }

    public void setEntry(List<Entry> entry) {
        this.entry = entry;
    }

    public List<Link> getLink() {
        return link;
    }

    public void setLink(List<Link> link) {
        this.link = link;
    }
}
