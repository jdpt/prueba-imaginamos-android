package co.imaginamos.pruebajuanpoveda.pojo;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class Artist {

    private String label;
    private ArtistAttrib attributes;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public ArtistAttrib getAttributes() {
        return attributes;
    }

    public void setAttributes(ArtistAttrib attributes) {
        this.attributes = attributes;
    }
}
