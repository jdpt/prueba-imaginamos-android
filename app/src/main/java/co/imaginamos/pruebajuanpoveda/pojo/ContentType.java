package co.imaginamos.pruebajuanpoveda.pojo;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class ContentType {

    private ContentTypeAttrib attributes;

    public ContentTypeAttrib getAttributes() {
        return attributes;
    }

    public void setAttributes(ContentTypeAttrib attributes) {
        this.attributes = attributes;
    }
}
