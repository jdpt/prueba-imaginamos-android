package co.imaginamos.pruebajuanpoveda.pojo;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class Author {

    private Label name;
    private Label uri;

    public Label getName() {
        return name;
    }

    public void setName(Label name) {
        this.name = name;
    }

    public Label getUri() {
        return uri;
    }

    public void setUri(Label uri) {
        this.uri = uri;
    }
}
