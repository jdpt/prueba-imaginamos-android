package co.imaginamos.pruebajuanpoveda.pojo;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class Id {

    private String label;
    private IdAttrib attributes;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public IdAttrib getAttributes() {
        return attributes;
    }

    public void setAttributes(IdAttrib attributes) {
        this.attributes = attributes;
    }
}
