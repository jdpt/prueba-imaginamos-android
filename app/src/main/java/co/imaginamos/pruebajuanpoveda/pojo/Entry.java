package co.imaginamos.pruebajuanpoveda.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class Entry {

    @SerializedName("im:name")
    private Label imName;
    @SerializedName("im:image")
    private List<ImImage> imImage;
    private Label summary;
    @SerializedName("im:price")
    private Price imPrice;
    @SerializedName("im:contentType")
    private ContentType imContentType;
    private Label rights;
    private Label title;
    private Link link;
    private Id id;
    @SerializedName("im:artist")
    private Artist imArtist;
    private Category category;
    @SerializedName("im:releaseDate")
    private ReleaseDate imReleaseDate;

    public Label getImName() {
        return imName;
    }

    public void setImName(Label imName) {
        this.imName = imName;
    }

    public List<ImImage> getImImage() {
        return imImage;
    }

    public void setImImage(List<ImImage> imImage) {
        this.imImage = imImage;
    }

    public Label getSummary() {
        return summary;
    }

    public void setSummary(Label summary) {
        this.summary = summary;
    }

    public Price getImPrice() {
        return imPrice;
    }

    public void setImPrice(Price imPrice) {
        this.imPrice = imPrice;
    }

    public ContentType getImContentType() {
        return imContentType;
    }

    public void setImContentType(ContentType imContentType) {
        this.imContentType = imContentType;
    }

    public Label getRights() {
        return rights;
    }

    public void setRights(Label rights) {
        this.rights = rights;
    }

    public Label getTitle() {
        return title;
    }

    public void setTitle(Label title) {
        this.title = title;
    }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Artist getImArtist() {
        return imArtist;
    }

    public void setImArtist(Artist imArtist) {
        this.imArtist = imArtist;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public ReleaseDate getImReleaseDate() {
        return imReleaseDate;
    }

    public void setImReleaseDate(ReleaseDate imReleaseDate) {
        this.imReleaseDate = imReleaseDate;
    }
}
