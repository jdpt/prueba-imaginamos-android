package co.imaginamos.pruebajuanpoveda.pojo;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class Price {

    private String label;
    private PriceAttrib attributes;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public PriceAttrib getAttributes() {
        return attributes;
    }

    public void setAttributes(PriceAttrib attributes) {
        this.attributes = attributes;
    }
}
