package co.imaginamos.pruebajuanpoveda.pojo;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class ReleaseDate {

    private String label;
    private Label attributes;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Label getAttributes() {
        return attributes;
    }

    public void setAttributes(Label attributes) {
        this.attributes = attributes;
    }
}
