package co.imaginamos.pruebajuanpoveda.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class IdAttrib {

    @SerializedName("im:id")
    private String imId;
    @SerializedName("im:bundleId")
    private String imBundleId;

    public String getImId() {
        return imId;
    }

    public void setImId(String imId) {
        this.imId = imId;
    }

    public String getImBundleId() {
        return imBundleId;
    }

    public void setImBundleId(String imBundleId) {
        this.imBundleId = imBundleId;
    }
}
