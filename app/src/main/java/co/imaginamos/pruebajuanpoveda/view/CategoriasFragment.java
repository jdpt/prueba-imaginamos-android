package co.imaginamos.pruebajuanpoveda.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.etsy.android.grid.StaggeredGridView;

import java.util.ArrayList;
import java.util.List;

import co.imaginamos.pruebajuanpoveda.ApplicationClass;
import co.imaginamos.pruebajuanpoveda.R;
import co.imaginamos.pruebajuanpoveda.adapter.CategoriasAdapter;
import co.imaginamos.pruebajuanpoveda.database.CategoriaDb;
import co.imaginamos.pruebajuanpoveda.db.DatabaseHelper;

public class CategoriasFragment extends android.support.v4.app.Fragment implements AbsListView.OnScrollListener,
        AbsListView.OnItemClickListener, AdapterView.OnItemLongClickListener, CategoriasAdapter.CategoriaClickListener{

    public static final String TAG = CategoriasFragment.class.getSimpleName();
    public static final String CATALOGO_ARG = "catalogo";
    private ArrayList<String> slididingImagesList = new ArrayList<>();
    private ListView listview;
    private List<CategoriaDb> mCategorias;
    private CategoriaClickListener listener;
    private ApplicationClass appClass;
    private DatabaseHelper dh;

    private StaggeredGridView mGridView;
    private boolean mHasRequestedMore;
    private CategoriasAdapter mAdapter;
    private ArrayList<String> mData;



    public CategoriasFragment() {
        // Required empty public constructor
    }

    public static CategoriasFragment newInstance() {

        CategoriasFragment f = new CategoriasFragment();
        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.i(TAG, "Attached CategoriasFragment");
        listener = (CategoriaClickListener) activity;
        appClass = (ApplicationClass) getActivity().getApplication();
        dh = appClass.getDbh();
        mCategorias = dh.findAllCategorias();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v;

        Log.i(TAG, "Catalogo Fragment Started!");

        v = inflater.inflate(R.layout.fragment_categorias, container, false);

        ((ActionBarActivity) getActivity()).getSupportActionBar().setTitle("Categories");

        System.out.println("tamaño cats: " + mCategorias.size());

        mGridView = (StaggeredGridView) v.findViewById(R.id.grid_view);

        LayoutInflater layoutInflater = getLayoutInflater(new Bundle());

        mAdapter = new CategoriasAdapter(getActivity(), R.id.txt_line1, this);

        //if (mData == null) {
        //    mData = SampleData.generateSampleData();
        //}

        mData = new ArrayList<>();
        //for (CategoriaDb cdb : cats) {
        for (CategoriaDb pdb : mCategorias) {
            mData.add(pdb.getNombre());
        }
        //}


        for (CategoriaDb data : mCategorias) {
            mAdapter.add(data);
        }

        mGridView.setAdapter(mAdapter);
        mGridView.setOnScrollListener(this);
        mGridView.setOnItemClickListener(this);
        mGridView.setOnItemLongClickListener(this);

        return v;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        return false;
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int i, int i1, int i2) {

    }

    @Override
    public void onCategoriaClick(CategoriaDb cat) {
        listener.onCategoriaClick(cat);
    }

    /* Interface for handling clicks - both normal and long ones. */
    public interface CategoriaClickListener {

        /**
         * Called when the view is clicked.
         *
         */
        public void onCategoriaClick(CategoriaDb cat);

    }

}