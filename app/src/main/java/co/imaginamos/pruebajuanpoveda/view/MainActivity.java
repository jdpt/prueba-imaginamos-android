package co.imaginamos.pruebajuanpoveda.view;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import co.imaginamos.pruebajuanpoveda.R;
import co.imaginamos.pruebajuanpoveda.database.CategoriaDb;
import co.imaginamos.pruebajuanpoveda.database.ProductoDb;
import co.imaginamos.pruebajuanpoveda.util.Utilidades;

public class MainActivity extends AppCompatActivity implements CategoriasFragment.CategoriaClickListener, ProductosFragment.ProductoClickListener,
        CarritoFragment.CarritoClickListener{

    public static String categoriaSeleccionada;
    CollapsingToolbarLayout collapsingToolbar;
    int mutedColor = R.attr.colorPrimary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(Utilidades.isTablet(this)){
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else{
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        final Toolbar toolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Categories");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, CategoriasFragment.newInstance(), "Categories")
                        //.addToBackStack(null)
                .commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCategoriaClick(CategoriaDb cat) {
        System.out.println("Categoria clicked!");

        Bundle args = new Bundle();
        args.putSerializable(ProductosFragment.PROD_ARG, cat);

        ProductosFragment f = new ProductosFragment();
        f.setArguments(args);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.bounce, R.anim.slide_up, R.anim.bounce, R.anim.slide_up)
                .add(R.id.container, f, "Productos")
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onProductoClick(ProductoDb prod) {
        System.out.println("Producto clicked!!");

        Bundle args = new Bundle();
        args.putSerializable(ProductoDetallesFragment.PROD_ARG, prod);

        ProductoDetallesFragment f = new ProductoDetallesFragment();
        f.setArguments(args);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.shrink_and_rotate_enter, R.anim.shrink_and_rotate_exit, R.anim.shrink_and_rotate_enter, R.anim.shrink_and_rotate_exit)
                .add(R.id.container, f)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        CategoriasFragment fc = (CategoriasFragment) getSupportFragmentManager().findFragmentByTag("Categories");
        ProductosFragment fp = (ProductosFragment) getSupportFragmentManager().findFragmentByTag("Productos");
        if (fp != null && fp.isVisible()) {
            getSupportActionBar().setTitle(categoriaSeleccionada);
            //super.onBackPressed();
        } else if (fc != null && fc.isVisible()) {
            getSupportActionBar().setTitle("Categories");
            //super.onBackPressed();
        } else {
            //super.onBackPressed();
        }
    }

    @Override
    public void onRemoveProductoCarritoClick(ProductoDb pdb) {

    }
}
