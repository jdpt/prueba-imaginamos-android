package co.imaginamos.pruebajuanpoveda.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;

import com.etsy.android.grid.StaggeredGridView;

import java.util.ArrayList;
import java.util.List;

import co.imaginamos.pruebajuanpoveda.ApplicationClass;
import co.imaginamos.pruebajuanpoveda.R;
import co.imaginamos.pruebajuanpoveda.adapter.ProductosAdapter;
import co.imaginamos.pruebajuanpoveda.database.CategoriaDb;
import co.imaginamos.pruebajuanpoveda.database.ProductoDb;
import co.imaginamos.pruebajuanpoveda.db.DatabaseHelper;


public class ProductosFragment extends android.support.v4.app.Fragment implements AbsListView.OnScrollListener,
        AbsListView.OnItemClickListener, AdapterView.OnItemLongClickListener, ProductosAdapter.ProductoClickListener{

    public static final String TAG = CategoriasFragment.class.getSimpleName();
    public static final String PROD_ARG = "productos";
    private CategoriaDb mCategoria;
    private List<ProductoDb> mProductos;
    private ProductoClickListener listener;
    private ApplicationClass appClass;
    private DatabaseHelper dh;

    private StaggeredGridView mGridView;
    private boolean mHasRequestedMore;
    private ProductosAdapter mAdapter;
    private ArrayList<String> mData;

    public ProductosFragment() {
        // Required empty public constructor
    }

    public static ProductosFragment newInstance() {

        ProductosFragment f = new ProductosFragment();
        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.i(TAG, "Attached ProductosFragment");
        listener = (ProductoClickListener) activity;
        appClass = (ApplicationClass) getActivity().getApplication();
        dh = appClass.getDbh();
        mCategoria = (CategoriaDb) getArguments().getSerializable(PROD_ARG);
        mProductos = dh.findProductosByCategoria(mCategoria.getId());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v;

        Log.i(TAG, "Productos Fragment Started!");

        v = inflater.inflate(R.layout.fragment_productos, container, false);

        ((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(mCategoria.getNombre());
        NavDrawerActivity.categoriaSeleccionada = mCategoria.getNombre();

        System.out.println("tamaño prods: " + mProductos.size());

        mGridView = (StaggeredGridView) v.findViewById(R.id.grid_view);

        LayoutInflater layoutInflater = getLayoutInflater(new Bundle());

        mAdapter = new ProductosAdapter(getActivity(), R.id.txt_line1, this);

        //if (mData == null) {
        //    mData = SampleData.generateSampleData();
        //}

        mData = new ArrayList<>();
        //for (CategoriaDb cdb : cats) {
        for (ProductoDb pdb : mProductos) {
            mData.add(pdb.getImage());
        }
        //}


        for (ProductoDb data : mProductos) {
            mAdapter.add(data);
        }

        mGridView.setAdapter(mAdapter);
        mGridView.setOnScrollListener(this);
        mGridView.setOnItemClickListener(this);
        mGridView.setOnItemLongClickListener(this);

        return v;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        return false;
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int i, int i1, int i2) {

    }

    @Override
    public void onProductoClick(ProductoDb pdb) {
        listener.onProductoClick(pdb);
    }

    /* Interface for handling clicks - both normal and long ones. */
    public interface ProductoClickListener {

        /**
         * Called when the view is clicked.
         *
         */
        public void onProductoClick(ProductoDb prod);

    }

}