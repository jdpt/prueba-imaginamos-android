package co.imaginamos.pruebajuanpoveda.view;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;

import co.imaginamos.pruebajuanpoveda.ApplicationClass;
import co.imaginamos.pruebajuanpoveda.R;
import co.imaginamos.pruebajuanpoveda.database.CategoriaDb;
import co.imaginamos.pruebajuanpoveda.database.ProductoDb;
import co.imaginamos.pruebajuanpoveda.db.DatabaseHelper;
import co.imaginamos.pruebajuanpoveda.pojo.Json;
import co.imaginamos.pruebajuanpoveda.util.Utilidades;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;


public class SplashActivity extends ActionBarActivity {

    private final int SPLASH_DISPLAY_LENGTH = 1400;
    private Json listCatalogo;
    private DatabaseHelper dh;
    private ApplicationClass appClass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if(Utilidades.isTablet(this)){
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else{
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        appClass = (ApplicationClass) getApplication();
        dh = appClass.getDbh();

        if(Utilidades.isConnected(this)) {

            new ConsultaCatalogo().execute();

        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                    Intent mainIntent = new Intent(SplashActivity.this, NavDrawerActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                    overridePendingTransition(R.anim.bounce,R.anim.bounce);
                    SplashActivity.this.finish();
                }
            }, SPLASH_DISPLAY_LENGTH);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ConsultaCatalogo extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            if (Utilidades.isConnected(SplashActivity.this)) {
                try {

                    Gson gson = new Gson();

                    Type listType = new TypeToken<Json>() {
                    }.getType();

                    InputStream json = getJson();

                    if (json != null) {

                        Reader reader = new InputStreamReader(json);

                        listCatalogo = gson.fromJson(reader, listType); //Si el 200

                        if(listCatalogo != null) {
                            dh.deleteProductos();
                            dh.deleteCategorias();
                            dh.insertCategorias(listCatalogo.getFeed());
                            dh.insertProductos(listCatalogo.getFeed());
                            System.out.println("*******Insertado el catalogo JSON en la bd!!!");

                        }

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("NO HAY CONEXION");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            System.out.println("Categorias Insertadas: " + dh.findAllCategorias().size());
            System.out.println("Productos Insertados: " + dh.findAllProductos().size());

            for(CategoriaDb cd : dh.findAllCategorias()){
                System.out.println("Productos " + cd.getNombre());
                for(ProductoDb pd : dh.findProductosByCategoria(cd.getId())){
                    System.out.println(pd.getName());
                }
            }
            System.out.println("Todos los prod insertados: " + dh.findAllProductos().size());
            for(ProductoDb p :  dh.findAllProductos()){
                System.out.println(p.getName() + " Cat: " + p.getCategoriaDb().getNombre() + " CatId: " + p.getCategoriaDb().getId());
            }

            new Handler().postDelayed(new Runnable(){
                @Override
                public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                    Intent mainIntent = new Intent(SplashActivity.this,NavDrawerActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                    overridePendingTransition(R.anim.bounce,R.anim.bounce);
                    SplashActivity.this.finish();
                }
            }, 1);
        }

        private InputStream getJson() throws IOException {

            HttpClient client = new DefaultHttpClient();

            HttpGet httpGet = new HttpGet(getString(R.string.url_catalogo));
            HttpResponse resp = client.execute(httpGet);

            System.out.println(resp.getStatusLine().getStatusCode());

            if (resp.getStatusLine().getStatusCode() != 200) {
                String res = EntityUtils.toString(resp.getEntity());

                System.out.println(resp.getStatusLine().getStatusCode() + " res=" + res);
                return null;
            }

            HttpEntity ent = resp.getEntity();
            return ent.getContent();
        }
    }
}
