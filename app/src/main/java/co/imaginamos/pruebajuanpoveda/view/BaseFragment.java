package co.imaginamos.pruebajuanpoveda.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.imaginamos.pruebajuanpoveda.ApplicationClass;
import co.imaginamos.pruebajuanpoveda.R;
import co.imaginamos.pruebajuanpoveda.db.DatabaseHelper;

/**
 * Created by JUAN DAVID on 26/10/2015.
 */
public class BaseFragment extends Fragment{

    public static final String POSICION_SELECCIONADA = "position";
    private static final String TAG = "BaseFragment";
    private int selectedPosition;
    private ApplicationClass appClass;
    private DatabaseHelper dh;

    public BaseFragment() {

    }

    public static BaseFragment newInstance(int position) {

        Bundle extras = new Bundle();
        extras.putSerializable(BaseFragment.POSICION_SELECCIONADA, position);
        BaseFragment f = new BaseFragment();
        f.setArguments(extras);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.i(TAG, "Base Fragment Attached!");
        selectedPosition = (int) getArguments().getSerializable(POSICION_SELECCIONADA);

        appClass = (ApplicationClass) getActivity().getApplication();
        dh = appClass.getDbh();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /* Inflate the layout for this fragment */
        View view = inflater.inflate(R.layout.fragment_base, container, false);

        FragmentTransaction transaction = getChildFragmentManager()
                .beginTransaction();

        switch (selectedPosition) {

            case 1:
                Log.i(TAG, "Position 1 selected!");

                transaction.replace(R.id.root_frame, CategoriasFragment.newInstance(), "Categories");
                transaction.addToBackStack(null);
                transaction.commit();
                break;

            case 2:
                Log.i(TAG, "Position 2 selected!");

                transaction.replace(R.id.root_frame, CarritoFragment.newInstance(), "Carrito");
                transaction.addToBackStack(null);
                transaction.commit();
                break;

        }

        return view;
    }

}