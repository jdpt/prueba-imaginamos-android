package co.imaginamos.pruebajuanpoveda.view;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import co.imaginamos.pruebajuanpoveda.ApplicationClass;
import co.imaginamos.pruebajuanpoveda.R;
import co.imaginamos.pruebajuanpoveda.database.ProductoDb;
import co.imaginamos.pruebajuanpoveda.db.DatabaseHelper;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class ProductoDetallesFragment extends android.support.v4.app.Fragment{

    public static final String TAG = ProductoDetallesFragment.class.getSimpleName();
    public static final String PROD_ARG = "producto";
    final static String PREFERENCE_CARRITO = "ListaProductosCarrito";
    SharedPreferences prefsCarrito;
    SharedPreferences.Editor edCarrito;
    private ApplicationClass appClass;
    private DatabaseHelper dh;
    private ProductoDb productoSeleccionado;

    public ProductoDetallesFragment() {
        // Required empty public constructor
    }

    public static ProductoDetallesFragment newInstance() {

        ProductoDetallesFragment f = new ProductoDetallesFragment();
        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.i(TAG, "Attached ProductoDetallesFragment");
        appClass = (ApplicationClass) getActivity().getApplication();
        dh = appClass.getDbh();
        productoSeleccionado = (ProductoDb) getArguments().getSerializable(PROD_ARG);
        prefsCarrito = getActivity().getSharedPreferences(PREFERENCE_CARRITO, Context.MODE_PRIVATE);
        edCarrito = prefsCarrito.edit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v;

        Log.i(TAG, "Producto Detalles Fragment Started!");

        v = inflater.inflate(R.layout.fragment_producto_detalles, container, false);

        ImageView icon = (ImageView) v.findViewById(R.id.productoDetaImageView);
        TextView tituloProducto = (TextView) v.findViewById(R.id.nombreProductoDetaTextView);
        TextView descripcionProducto = (TextView) v.findViewById(R.id.detallesProductoTextView);
        TextView precioProducto = (TextView) v.findViewById(R.id.precioDetaTextView);
        TextView desarrolladorProducto = (TextView) v.findViewById(R.id.nombreDesarrolladorDetaTextView);

        Picasso.with(getActivity())
                .load(productoSeleccionado.getImage())
                .into(icon);

        tituloProducto.setText(productoSeleccionado.getName());
        descripcionProducto.setText(productoSeleccionado.getSummary());
        precioProducto.setText(productoSeleccionado.getCurrency() + " " + productoSeleccionado.getPrice());
        desarrolladorProducto.setText(productoSeleccionado.getArtist());

        ImageView addCart = (ImageView) v.findViewById(R.id.addCartImageView);
        addCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edCarrito.putLong(productoSeleccionado.getId().toString(), productoSeleccionado.getId());
                edCarrito.commit();
                Toast.makeText(getContext(), "Added to Shopping Cart!", Toast.LENGTH_SHORT).show();
            }
        });

        return v;
    }


}
