package co.imaginamos.pruebajuanpoveda.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import co.imaginamos.pruebajuanpoveda.R;
import co.imaginamos.pruebajuanpoveda.database.CategoriaDb;
import co.imaginamos.pruebajuanpoveda.database.ProductoDb;
import co.imaginamos.pruebajuanpoveda.util.Utilidades;

public class NavDrawerActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks,
        CategoriasFragment.CategoriaClickListener, ProductosFragment.ProductoClickListener, CarritoFragment.CarritoClickListener {

    public static String categoriaSeleccionada;
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_drawer);

        if(Utilidades.isTablet(this)){
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else{
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, BaseFragment.newInstance(position + 1))
                        //.addToBackStack(null)
                .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.nav_drawer, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRemoveProductoCarritoClick(ProductoDb pdb) {

    }

    @Override
    public void onCategoriaClick(CategoriaDb cat) {
        System.out.println("Categoria clicked!");

        Bundle args = new Bundle();
        args.putSerializable(ProductosFragment.PROD_ARG, cat);

        ProductosFragment f = new ProductosFragment();
        f.setArguments(args);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.bounce, R.anim.slide_up, R.anim.bounce, R.anim.slide_up)
                .add(R.id.container, f, "Productos")
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onProductoClick(ProductoDb prod) {
        System.out.println("Producto clicked!!");

        Bundle args = new Bundle();
        args.putSerializable(ProductoDetallesFragment.PROD_ARG, prod);

        ProductoDetallesFragment f = new ProductoDetallesFragment();
        f.setArguments(args);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.shrink_and_rotate_enter, R.anim.shrink_and_rotate_exit, R.anim.shrink_and_rotate_enter, R.anim.shrink_and_rotate_exit)
                .add(R.id.container, f)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onBackPressed() {

        System.out.println("back pressed main!");
        FragmentManager fm = getSupportFragmentManager();

        if (mNavigationDrawerFragment.isDrawerOpen()) {
            mNavigationDrawerFragment.closeDrawer(Gravity.START);
        } else if(!mNavigationDrawerFragment.isDrawerOpen() && fm.getBackStackEntryCount() == 0){
            mNavigationDrawerFragment.openDrawer(Gravity.START);
        } else {

                 System.out.println("BackStack count: " + fm.getBackStackEntryCount());

                if (fm.getBackStackEntryCount() > 0) {
                    super.onBackPressed();
                } else {
                    new AlertDialog.Builder(this)
                            .setTitle("Confirmacion")
                            .setMessage("Realmente desea salir de la app?")
                            .setNegativeButton("No", null)
                            .setPositiveButton("Si", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface arg0, int arg1) {
                                    NavDrawerActivity.super.onBackPressed();
                                }
                            }).create().show();
                }


        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_nav_drawer, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((NavDrawerActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}
