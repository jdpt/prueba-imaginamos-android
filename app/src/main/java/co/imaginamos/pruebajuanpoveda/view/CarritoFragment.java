package co.imaginamos.pruebajuanpoveda.view;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;

import com.etsy.android.grid.StaggeredGridView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import co.imaginamos.pruebajuanpoveda.ApplicationClass;
import co.imaginamos.pruebajuanpoveda.R;
import co.imaginamos.pruebajuanpoveda.adapter.CarritoAdapter;
import co.imaginamos.pruebajuanpoveda.database.ProductoDb;
import co.imaginamos.pruebajuanpoveda.db.DatabaseHelper;

/**
 * Created by JUAN DAVID on 26/10/2015.
 */
public class CarritoFragment extends android.support.v4.app.Fragment implements AbsListView.OnScrollListener,
        AbsListView.OnItemClickListener, AdapterView.OnItemLongClickListener, CarritoAdapter.CarritoClickListener{


    public static final String TAG = CarritoFragment.class.getSimpleName();
    final static String PREFERENCE_CARRITO = "ListaProductosCarrito";
    SharedPreferences prefsCarrito;
    SharedPreferences.Editor edCarrito;
    private List<ProductoDb> mProductos = new ArrayList<>();
    private ApplicationClass appClass;
    private DatabaseHelper dh;
    private StaggeredGridView mGridView;
    private CarritoClickListener listener;
    private boolean mHasRequestedMore;
    private CarritoAdapter mAdapter;
    private ArrayList<String> mData;

    public CarritoFragment() {
        // Required empty public constructor
    }

    public static CarritoFragment newInstance() {

        CarritoFragment f = new CarritoFragment();
        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.i(TAG, "Attached CarritoFragment");
        listener = (CarritoClickListener) activity;
        appClass = (ApplicationClass) getActivity().getApplication();
        dh = appClass.getDbh();
        prefsCarrito = getActivity().getSharedPreferences(PREFERENCE_CARRITO, Context.MODE_PRIVATE);
        edCarrito = prefsCarrito.edit();

        Collection<Long> allProductos = (Collection<Long>) prefsCarrito.getAll().values();

        for (Long s : allProductos) {
            mProductos.add(dh.findProductoById(s));
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v;

        Log.i(TAG, "Productos Fragment Started!");

        v = inflater.inflate(R.layout.fragment_carrito, container, false);

        ((ActionBarActivity) getActivity()).getSupportActionBar().setTitle("Shopping Cart");

        System.out.println("tamaño prods: " + mProductos.size());

        mGridView = (StaggeredGridView) v.findViewById(R.id.grid_view);

        LayoutInflater layoutInflater = getLayoutInflater(new Bundle());

        mAdapter = new CarritoAdapter(getActivity(), R.id.txt_line1, this);

        //if (mData == null) {
        //    mData = SampleData.generateSampleData();
        //}

        mData = new ArrayList<>();
        //for (CategoriaDb cdb : cats) {
        for (ProductoDb pdb : mProductos) {
            mData.add(pdb.getImage());
        }
        //}


        for (ProductoDb data : mProductos) {
            mAdapter.add(data);
        }

        mGridView.setAdapter(mAdapter);
        mGridView.setOnScrollListener(this);
        mGridView.setOnItemClickListener(this);
        mGridView.setOnItemLongClickListener(this);

        return v;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        return false;
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int i, int i1, int i2) {

    }

    @Override
    public void onRemoveProductoCarritoClick(ProductoDb pdb) {
        edCarrito.remove(pdb.getId().toString());
        edCarrito.commit();

        mProductos.clear();

        Collection<Long> allProductos = (Collection<Long>) prefsCarrito.getAll().values();

        for (Long s : allProductos) {
            mProductos.add(dh.findProductoById(s));
        }

        mData = new ArrayList<>();
        //for (CategoriaDb cdb : cats) {
        for (ProductoDb pd : mProductos) {
            mData.add(pd.getImage());
        }
        //}

        mAdapter.clear();

        for (ProductoDb data : mProductos) {
            mAdapter.add(data);
        }

        mAdapter.notifyDataSetChanged();
    }

    public interface CarritoClickListener {

        /**
         * Called when the view is clicked.
         *
         */
        //public void onAddProductoCarritoClick(ProductoDb pdb);
        public void onRemoveProductoCarritoClick(ProductoDb pdb);

    }
}
