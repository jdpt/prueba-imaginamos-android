package co.imaginamos.pruebajuanpoveda.adapter;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import co.imaginamos.pruebajuanpoveda.R;
import co.imaginamos.pruebajuanpoveda.database.CategoriaDb;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class CategoriasAdapter extends ArrayAdapter<CategoriaDb> {

    private static final String TAG = "CategoriasAdapter";
    private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();
    private final LayoutInflater mLayoutInflater;
    private final Random mRandom;
    private final ArrayList<Integer> mBackgroundColors;
    private CategoriaClickListener listener;

    public CategoriasAdapter(final Context context, final int textViewResourceId, CategoriaClickListener listener) {
        super(context, textViewResourceId);
        this.listener = listener;
        mLayoutInflater = LayoutInflater.from(context);
        mRandom = new Random();
        mBackgroundColors = new ArrayList<Integer>();
        mBackgroundColors.add(R.color.orange);
        mBackgroundColors.add(R.color.dark_green);
        mBackgroundColors.add(R.color.red);
        mBackgroundColors.add(R.color.dark_blue);
        mBackgroundColors.add(R.color.purple);
        mBackgroundColors.add(R.color.green);
        mBackgroundColors.add(R.color.blue);
        mBackgroundColors.add(R.color.pink);

    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final ViewHolder vh;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.list_categorias, parent, false);
            vh = new ViewHolder();
            vh.txtLineOne = (TextView) convertView.findViewById(R.id.txt_line1);

            convertView.setTag(vh);
        }
        else {
            vh = (ViewHolder) convertView.getTag();
        }

        double positionHeight = getPositionRatio(position);
        int backgroundIndex = position >= mBackgroundColors.size() ?
                position % mBackgroundColors.size() : position;

        convertView.setBackgroundResource(mBackgroundColors.get(backgroundIndex));

        //vh.txtLineOne.setHeightRatio(positionHeight);
        //vh.txtLineOne.setImageURI("");//setText(getItem(position) + position);

        //Picasso.with(getContext()).load(getItem(position).getImagen1()).placeholder(R.drawable.progress_animation).into(vh.txtLineOne);

        vh.txtLineOne.setText(getItem(position).getNombre());

        vh.txtLineOne.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {
                //Toast.makeText(getContext(), "Touch en imagen # " + position, Toast.LENGTH_SHORT).show();
                listener.onCategoriaClick(getItem(position));
                final Dialog dialog = new Dialog(getContext());
            }
        });

        return convertView;
    }

    private double getPositionRatio(final int position) {
        double ratio = sPositionHeightRatios.get(position, 0.0);
        // if not yet done generate and stash the columns height
        // in our real world scenario this will be determined by
        // some match based on the known height and width of the image
        // and maybe a helpful way to get the column height!
        if (ratio == 0) {
            ratio = getRandomHeightRatio();
            sPositionHeightRatios.append(position, ratio);
            Log.d(TAG, "getPositionRatio:" + position + " ratio:" + ratio);
        }
        return ratio;
    }

    private double getRandomHeightRatio() {
        return (mRandom.nextDouble() / 2.0) + 1.0; // height will be 1.0 - 1.5 the width
    }

    /* Interface for handling clicks - both normal and long ones. */
    public interface CategoriaClickListener {

        /**
         * Called when the view is clicked.
         *
         */
        public void onCategoriaClick(CategoriaDb cat);

    }

    static class ViewHolder {
        TextView txtLineOne;
    }
}