package co.imaginamos.pruebajuanpoveda.adapter;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.etsy.android.grid.util.DynamicHeightImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

import co.imaginamos.pruebajuanpoveda.R;
import co.imaginamos.pruebajuanpoveda.database.ProductoDb;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class ProductosAdapter extends ArrayAdapter<ProductoDb> {

    private static final String TAG = "ProductosAdapter";
    private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();
    private final LayoutInflater mLayoutInflater;
    private final Random mRandom;
    private final ArrayList<Integer> mBackgroundColors;
    private ProductoClickListener listener;

    public ProductosAdapter(final Context context, final int textViewResourceId, ProductoClickListener listener) {
        super(context, textViewResourceId);
        mLayoutInflater = LayoutInflater.from(context);
        mRandom = new Random();
        this.listener = listener;
        mBackgroundColors = new ArrayList<Integer>();
        //mBackgroundColors.add(R.color.orange);
        //mBackgroundColors.add(R.color.green);
        //mBackgroundColors.add(R.color.blue);
        //mBackgroundColors.add(R.color.yellow);
        mBackgroundColors.add(R.color.grey);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final ViewHolder vh;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.list_productos, parent, false);
            vh = new ViewHolder();
            vh.txtLineOne = (DynamicHeightImageView) convertView.findViewById(R.id.txt_line1);
            vh.loading = (ProgressBar) convertView.findViewById(R.id.progressBarLoadingProducto);
            vh.nombreDesarrollador = (TextView) convertView.findViewById(R.id.nombreDesarrolladorTextView);
            vh.nombreProducto = (TextView) convertView.findViewById(R.id.nombreProductoTextView);
            vh.productoRelativeLayout = (RelativeLayout) convertView.findViewById(R.id.productoRelativeLayout);
            vh.precioProducto = (TextView) convertView.findViewById(R.id.precioProductoTextView);

            convertView.setTag(vh);
        }
        else {
            vh = (ViewHolder) convertView.getTag();
        }

        double positionHeight = getPositionRatio(position);
        int backgroundIndex = position >= mBackgroundColors.size() ?
                position % mBackgroundColors.size() : position;

        convertView.setBackgroundResource(mBackgroundColors.get(backgroundIndex));

        //vh.txtLineOne.setHeightRatio(positionHeight);
        //vh.txtLineOne.setImageURI("");//setText(getItem(position) + position);

        //Picasso.with(getContext()).load(getItem(position).getImagen1()).placeholder(R.drawable.progress_animation).into(vh.txtLineOne);

        vh.nombreProducto.setText(getItem(position).getName());
        vh.precioProducto.setText(getItem(position).getCurrency() + " " + getItem(position).getPrice());
        vh.nombreDesarrollador.setText(getItem(position).getArtist());

        Picasso.with(getContext())
                .load(getItem(position).getImage())
                .into(vh.txtLineOne, new Callback() {
                    @Override
                    public void onSuccess() {
                        vh.loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        // TODO Auto-generated method stub

                    }
                });
        vh.productoRelativeLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {
                //Toast.makeText(getContext(), "Touch en producto # " + position, Toast.LENGTH_SHORT).show();
                listener.onProductoClick(getItem(position));

            }
        });

        return convertView;
    }

    private double getPositionRatio(final int position) {
        double ratio = sPositionHeightRatios.get(position, 0.0);
        // if not yet done generate and stash the columns height
        // in our real world scenario this will be determined by
        // some match based on the known height and width of the image
        // and maybe a helpful way to get the column height!
        if (ratio == 0) {
            ratio = getRandomHeightRatio();
            sPositionHeightRatios.append(position, ratio);
            Log.d(TAG, "getPositionRatio:" + position + " ratio:" + ratio);
        }
        return ratio;
    }

    private double getRandomHeightRatio() {
        return (mRandom.nextDouble() / 2.0) + 1.0; // height will be 1.0 - 1.5 the width
    }

    public interface ProductoClickListener {

        /**
         * Called when the view is clicked.
         *
         */
        public void onProductoClick(ProductoDb pdb);

    }

    static class ViewHolder {
        DynamicHeightImageView txtLineOne;
        ProgressBar loading;
        TextView nombreProducto;
        TextView nombreDesarrollador;
        RelativeLayout productoRelativeLayout;
        TextView precioProducto;
    }
}
