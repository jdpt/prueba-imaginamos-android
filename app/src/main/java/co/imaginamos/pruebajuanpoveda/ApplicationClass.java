package co.imaginamos.pruebajuanpoveda;

import android.app.Application;

import co.imaginamos.pruebajuanpoveda.db.DatabaseHelper;

/**
 * Created by JUAN DAVID on 24/10/2015.
 */
public class ApplicationClass extends Application {

    private final String TAG = ApplicationClass.class.getSimpleName();
    private DatabaseHelper dbh;

    @Override
    public void onCreate() {
        super.onCreate();
        dbh = new DatabaseHelper(this);
    }

    public DatabaseHelper getDbh() {
        return dbh;
    }

}